
// THE NAMES WITH LETTER "A"

db.users.find(
    {
        $or: [
        {"firstName": { $regex: "A", $options: "i"}},
        {"lastName": { $regex: "A", $options: "i"}}
        ]
    },
    {
        "firstName": 1,
        "lastName": 1,
        "_id": 0
    }
);


// USERS isAdmin and isActive == true

db.users.find(
    {
        $and: [
            {"isAdmin": true},
            {"isActive": true}
]});


// Courses with letter "u" and price greater than or equal to 13000

db.courses.find(
    {
        $and: [
        {"name": { $regex: "U", $options: "i"}},
        {"price": {$gte: 13000}}
]});

